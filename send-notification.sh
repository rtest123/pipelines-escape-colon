#!/bin/bash
#---------------------------------------------------------------------------
curl -X POST "https://webhook.site/56235a2e-6918-45a5-8056-17637e358b1b" \
     -H "X-Api-Key:'"$API_KEY"'" \
     -i \
     -H "Content-Type: application/json" \
     -d \
'{
  "deployment": {
    "revision": "'"$BITBUCKET_COMMIT"'",
    "changelog": "Added: /v2/deployments.rb, Removed: None",
    "description": "'"$BITBUCKET_DEPLOYMENT_ENVIRONMENT"'",
    "user": "datanerd@example.com",
    "timestamp": "'"$(date)"'"
  }
}' 
#---------------------------------------------------------------------------